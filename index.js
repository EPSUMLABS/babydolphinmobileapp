/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Index from './js/index.js';

AppRegistry.registerComponent(appName, () => Index);
