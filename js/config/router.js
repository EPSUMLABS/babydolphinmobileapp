import {StackNavigator, DrawerNavigator} from 'react-navigation';
import React from 'react';
import { Platform, Dimensions } from 'react-native';
import Splash from '../screens/Splash';
import Home from '../screens/Home';
import Login from '../screens/Login';
import Book from '../screens/Book';
import Select from '../screens/Select';
import Profile from '../screens/Profile';
import Viewbooking from '../screens/Viewbooking';
import Ordersummary from '../screens/Ordersummary';
import Bookingdetails from '../screens/Bookingdetails';
import descriptionpage from '../screens/descriptionpage';
import Confirm from '../screens/Confirm';
import Aboutus from '../screens/Aboutus';
import Logout from '../screens/Logout';
import DolphinPoint from '../screens/DolphinPoint';
import KaliJai from '../screens/KaliJai';
import BirdSanctuary from '../screens/BirdSanctuary';
import Island from '../screens/Island';
import Privacypolicy from '../screens/Privacypolicy';
import Termandcondition from '../screens/Termandcondition';
import Sidebar from '../screens/Sidebar';
import Packages from '../screens/Packages';

const screenWidth = Dimensions.get("window").width;

const Drawer=(
  {
    Home:{screen:Home},
    Book:{screen:Book},
    Viewbooking:{screen:Viewbooking},
    Bookingdetails:{screen:Bookingdetails},
    Ordersummary:{screen:Ordersummary},
    descriptionpage:{screen:descriptionpage},
    Confirm:{screen:Confirm},
    Select:{screen:Select},
    Profile:{screen:Profile},
    DolphinPoint:{screen:DolphinPoint},
    KaliJai:{screen:KaliJai},
    BirdSanctuary:{screen:BirdSanctuary},
    Island:{screen:Island},
    Aboutus:{screen:Aboutus},
    Termandcondition:{screen:Termandcondition},
    Privacypolicy:{screen:Privacypolicy},
    Packages:{screen:Packages}
  })

  const NavigationMenu = DrawerNavigator(
    Drawer,
    {
      initialRouteName:'Profile',
      drawerWidth: screenWidth - (Platform.OS === 'android' ? 56 : (screenWidth >414 ? 500 : 64)),
      contentComponent: props => <Sidebar {...props} routes={Drawer}/>,
      drawerPosition:'left',
    });


  const StackNav=StackNavigator(
    {
     Splash:{screen:Splash},
     Login:{screen:Login},
     NavigationMenu:{screen:NavigationMenu},
     Logout:{screen:Logout}
    },
  {headerMode:'none'},
  {
    initialRouteName: 'Home',
  });
  
  export default StackNav;
   