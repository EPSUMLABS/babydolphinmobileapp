import React, { Component } from 'react';
import {  View, Text, TouchableOpacity, Dimensions, StatusBar, AsyncStorage, BackHandler, Image, Alert} from 'react-native';
import { Container,Content, Header,Title,Left, Body, Right, Icon,Thumbnail, Footer, FooterTab} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;

export default class Profile extends Component{
  state={
    open:false,
    name:'',
    email:'',     
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }

  componentWillMount(){
    AsyncStorage.getItem('user')
    .then((value) => {
      const values = JSON.parse(value)
      this.setState({name:values.name, email:values.email });
  });
}


handle_logout(){
  Alert.alert(
    'Are you sure?',
    'Do you want to Logout?',
    [
      {text: 'YES', onPress: () => this.props.navigation.navigate('Logout')},
      {text: 'NO', onPress: () => console.log("no")},
    ],
    { cancelable: false }
  )  
}
    render() {
        return (
          <Container>
            <Header style={{ backgroundColor: '#df3a45' }}>
              <Left>
                <TouchableOpacity transparent onPress={() => this.props.navigation.openDrawer()}>
                  <Icon type="AntDesign" name='bars' style={{ fontSize: 25, color: '#fff' }} />
                </TouchableOpacity>
              </Left>
              <Body><Title>My Profile</Title></Body>
              <Right />
            </Header>
            <StatusBar backgroundColor="#df3a45" barStyle="light-content" />
            <Content>
              <Image source={require('../assets/p_bg.jpg')} style={{ height: screenHeight / 3 }} ></Image>
              <View style={{ marginTop: -30, alignItems: 'center' }}>
                <Thumbnail source={require('../assets/user.png')} style={{ width: 55, backgroundColor: '#000' }} />
              </View>

              <View style={{ alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                
                <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: screenwidth - 20, height: screenHeight / 17, borderRadius: 10, alignContent: 'center', alignItems: 'center' }}>
                  <Icon style={{ fontSize: 20, paddingLeft: 10, color: '#e61610' }} type="Entypo" name='user' />
                  <Text style={{ paddingLeft: '10%', fontSize: 15, color: '#000' }}>{this.state.name}</Text>
                </View>

                <View style={{ flexDirection: 'row', backgroundColor: '#fff', width: screenwidth - 20, height: screenHeight / 17, borderRadius: 10, alignContent: 'center', alignItems: 'center' }}>
                  <Icon style={{ fontSize: 20, paddingLeft: 10, color: '#e61610' }} type="Zocial" name='email' />
                  <Text style={{ paddingLeft: '10%', fontSize: 15, color: '#000' }}>{this.state.email}</Text>
                </View>

                <View style={{ borderBottomColor: '#bfbfbf', borderBottomWidth: 1, width: screenwidth - 40, margin: 20 }} />

                <TouchableOpacity onPress={() => this.handle_logout()}
                  style={{
                    flexDirection: 'row', backgroundColor: '#fff', width: screenwidth - 20,
                    height: screenHeight / 17, borderRadius: 10, alignContent: 'center', alignItems: 'center'
                  }}>
                  <Icon type="Ionicons" name='ios-power' style={{ fontSize: 20, paddingLeft: 10, color: '#e61610' }} />
                  <Text style={{ paddingLeft: '10%', fontSize: 15, color: '#000' }}>Logout</Text>
                </TouchableOpacity>
              </View>
            </Content>
            <Footer>
              <FooterTab style={{ backgroundColor: '#fff' }}>
                <LinearGradient colors={['#7b4397', '#dc2430']} start={{ x: 1, y: 0.1 }} end={{ x: 0.0, y: 0.25 }} style={{ width: screenwidth, alignItems: "center", justifyContent: "center", flexDirection: 'row' }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate(("Viewbooking"),
                    { packagedetails: this.state.packagedetails })} style={{ flexDirection: 'row', alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ color: '#fff', textAlign: 'center' }}>My Bookings</Text>
                    <Icon style={{ color: '#fff', alignItems: 'center', paddingLeft: 10 }} name='ios-arrow-round-forward' />
                  </TouchableOpacity>
                </LinearGradient>
              </FooterTab>
            </Footer>
          </Container>
 
            );
  }
}
