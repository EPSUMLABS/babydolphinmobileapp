

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
  import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator, Badge,
    Item, Input, Spinner, Card, CardItem,
  } from 'native-base';
  import LinearGradient from 'react-native-linear-gradient';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class Island extends Component {
    state = {
        spinner: true,
      };
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Home");
        return true;
      };
  render() {
    return (
    <Container>
          <Header style={{ backgroundColor:'#df3a45' }} >
          <Left>
          <Icon name='arrow-back' onPress={()=>this.props.navigation.navigate("Home")}
             style={{ fontSize: 25, color: '#fff' }} />
          </Left>
          <Body><Title>Island</Title></Body>
          <Right>
          </Right>
        </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
     <Content>
   <View style={{alignContent:'center',alignItems:'center'}}>
   <Image source={require('../assets/island.jpg')} style={{width:screenwidth,height:screenHeight/3}}/>
   {/* <ImageBackground source={{uri:'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Kalijai_temple_Chilika.jpg/1200px-Kalijai_temple_Chilika.jpg'}} style={{width:screenwidth,height:screenHeight/3}}>
    
          <Icon name='arrow-back' style={{ fontSize: 25, color: '#fff',paddingLeft:4,paddingTop:10 }} onPress={()=>this.onBackPress()}/>
  
</ImageBackground> */}
<View style={{paddingTop:10}}>
<Text style={{fontWeight:'bold', textAlign:'center'}}>Nirmaljhara Island</Text>
<Text style={{padding:15}}>
one of the real cookout spot in the midst of the salty water tidal pond, Nirmaljhara has fanciful essentialness as it is accepted to be associated with Lord Vishnu. A stream streams out of the picture of Lord Vishnu which is known as Nirmaljahara.
</Text>
<Text style={{fontWeight:'bold', textAlign:'center',marginTop:10}}>Sankunda Island</Text>
<Text style={{padding:15}}>
Shankh or Breakfast Island-these are the monikers given to Sankunda Island. Rich flower species can be found at this spot.
</Text>
<Text style={{fontWeight:'bold', textAlign:'center',marginTop:10}}>Fowl Island</Text>
<Text style={{padding:15}}>
It is a noteworthy spot which is visited by a large number of winged creature species and in this way is a critical center point for fledgling watchers. Uncommon herbs, trees, creepers, and bushes include the bio-assorted variety of Bird Island and white stone rocks are among the exceptional attractions.
</Text>
    </View>
       </View>
      </Content>
      <Footer>
          <FooterTab style={{ backgroundColor: '#fff' }}>
          <LinearGradient colors={['#7b4397', '#dc2430']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center"}}>
            <TouchableOpacity  onPress={() =>this.props.navigation.navigate(("DolphinPoint"),
                  {packagedetails:this.state.packagedetails})}style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}>
                <Text style={{ color: '#fff',textAlign:'center'}}>NEXT</Text>
                <Icon style={{ color: '#fff',  alignItems:'center', paddingLeft:10 }} name='ios-arrow-round-forward' />
            </TouchableOpacity>
            </LinearGradient>
          </FooterTab>
        </Footer>
      </Container>
  );
}
}



 

