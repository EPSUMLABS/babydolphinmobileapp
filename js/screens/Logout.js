import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import { Spinner } from 'native-base';
// create a component
class Logout extends Component {
    componentWillMount(){
        AsyncStorage.setItem('user',"");
        this.props.navigation.navigate('Login');
    }
    render() {
        return (
            <View style={styles.container}>
           
            <Spinner color='red' />
              
           
            </View>
            
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default Logout;
