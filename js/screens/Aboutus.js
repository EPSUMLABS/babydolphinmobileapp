

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
  import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator, Badge,
    Item, Input, Spinner, Card, CardItem,
  } from 'native-base';
  import LinearGradient from 'react-native-linear-gradient';
  const screenWidth = Dimensions.get('window').width;
  const screenHeight = Dimensions.get('window').height;
export default class Splash extends Component {
    state = {
        spinner: true,
      };
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Home");
        return true;
      };
  render() {
    return (
      <Container>
      <Header style={{ backgroundColor: '#df3a45' }}>
        <Left>
          <TouchableOpacity transparent onPress={() => this.props.navigation.openDrawer()}>
            <Icon type="AntDesign" name='bars' style={{ fontSize: 25, color: '#fff' }} />
          </TouchableOpacity>
        </Left>
        <Body><Title>Contact Us</Title></Body>
        <Right/>
      </Header>
      <StatusBar backgroundColor="#df3a45" barStyle="light-content" />
      <Content style={{ backgroundColor: '#fff', }}>
        <View>
        <Text style={{fontSize:24,textAlign:'center', fontWeight: 'bold',marginTop:10}}>About US </Text>
        <Text style={{margin:10, fontSize:16}}>The largest brackish water lagoon lake of Asia.Chilika Lake is situated on the beachfront of Bay of Bengal on Odisha offers unspoiled and all encompassing view particularly for birdwatchers.From the mouth of the Daya River, it streams into the Bay of Bengal.</Text>
        <View style={{ borderBottomColor: '#bfbfbf', borderBottomWidth: 1, width:screenWidth-40, margin:20}} />
        <Text style={{fontSize:24,textAlign:'center', fontWeight: 'bold',marginTop:10}}>Contact US </Text>
        
        <View style={{flexDirection:'row', width:screenWidth-40, marginLeft: 10, marginTop:10}}>
         <Icon type="Entypo" name='address' style={{ color: '#df3a45', fontSize:25 }} />
         <Text style={{marginLeft:5, marginRight:5, fontSize:16}}>Chilika, Satapada, Khordha, Puri, Odisha India</Text>
         </View>

         <View style={{flexDirection:'row', margin: 10}}>
         <Icon type="Zocial" name='email' style={{ color: '#df3a45', fontSize:20 }} />
         <Text style={{marginLeft:5,fontSize:16}}>babydolphin.in@gmail.com</Text>
         </View>

         <View style={{flexDirection:'row', margin: 10}}>
         <Icon type="Entypo" name='mobile' style={{ color: '#df3a45', fontSize:20 }} />
         <Text style={{marginLeft:5,fontSize:16}}>+91 7978955409</Text>
         </View>
        
        </View>
      </Content>
    </Container>

  );
}
}



 

