import React, { Component } from 'react';
import {  View, Text, TouchableOpacity, Dimensions, StatusBar,Linking,AsyncStorage, Alert, ImageBackground, BackHandler,Image} from 'react-native';
import { Container,Content, Header, Left, Body, Right,Icon, Thumbnail,Item, Input,Spinner} from 'native-base';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import FBSDK, {LoginManager, AccessToken, GraphRequest, GraphRequestManager} from 'react-native-fbsdk';
import urldetails from '../config/endpoints.json';
import LinearGradient from 'react-native-linear-gradient';
import Modal from "react-native-simple-modal";
import { GoogleSignin, GoogleSigninButton } from 'react-native-google-signin';
import { validateEmail, validatePassword } from '../config/validate';
export default class Login extends Component {
  state={
    open:false,
    mobile:'',
    email:'',
    password:'',
    spinner: false,
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '673880869624-mv0mq9hpv2gt41kkt6d6uuofm7ij3n47.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      //offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      //hostedDomain: '', // specifies a hosted domain restriction
      //forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
      //accountName: '', // [Android] specifies an account name on the device that should be used
    });

  }
  onBackPress = () => {
    BackHandler.exitApp()
  };
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  moveUp = () => this.setState({ offset: -100 });
  resetPosition = () => this.setState({ offset: 0 });
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });
  loginUser(){
    if (this.state.email == "") {
      alert('Email field cannot be blank !')
    }
    // else if (!validateEmail(this.state.email)) {
    //   alert('Please enter valid email !')
    // }
     else if (this.state.password == "") {
      alert('Password cannot be blank !')
    }
    // else if (!validatePassword(this.state.password)) {
    //   alert('Password should contain one uppercase letter,one digit and should be of 6 characters ..')
    // }
     else {
    this.setState({open:true})
    var formData= new FormData();
    formData.append('user_email',this.state.email);
    formData.append('user_password',this.state.password);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["userlogin"],{
      method:"POST",
      body:formData
    })
    .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      // Alert.alert('',jsondata.message)
      this.setState({open:false})
      this.props.navigation.navigate("Home")
      var userdetails={
        userid:jsondata.data['user_id'],
        username:jsondata.data['user_name'],
        email:jsondata.data['user_email'],
        contact:jsondata.data['user_contact'],
        password:jsondata.data['user_password']
      }
      AsyncStorage.setItem('user',JSON.stringify(userdetails));

    }
    else if(jsondata.status=="failed"){
      Alert.alert(
        'Alert',
        "Your email address and password is incorrect.Please enter valid email and password .")
    }
    this.setState({ open: false })
}
) .catch((error) => {
  console.log(error)
  Alert.alert(
    'Alert',
    "Please check your Internet Connection.",
    [
      {text: 'Retry', onPress: () =>this.loginUser()},
    ],
    { cancelable: false }
  )
});
  }
}

googlesignIn=async ()=> {
  try {
    const userInfo = await GoogleSignin.signIn();
    this.sendsocialinfo(userInfo.user.id,userInfo.user.email,userInfo.user.name,"Google")
  } catch (error) {
    console.log(error);
    alert("Login Cancelled")
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // user cancelled the login flow
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (f.e. sign in) is in progress already
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
    } else {
      // some other error happened
    }
  }
}


_fbLogin(){
  this.setState({ spinner: true })
  LoginManager.logInWithPermissions(['public_profile','email'])
  .then((result)=>{
    if(result.isCancelled){
      alert("Login is cancelled "+result.isCancelled)
      this.setState({ spinner: false })
    }else{
        AccessToken.getCurrentAccessToken()
        .then((data)=>{
          let accessToken = data.accessToken
          this.setState({name:data.accessToken.toString()})

          const responseInfoCallback = (error,result)=>{
            if(error){
              alert(error)
              //this.setState({ spinner: false })
            }else{
              console.log("fb login success")
              console.log(result.id+" , "+result.email+" , "+result.name)
              this.sendsocialinfo(result.id,result.email,result.name,"Facebook")
            }
          }
          const infoRequest = new GraphRequest('/me',{
            accessToken:accessToken,
            parameters:{
              fields:{
                string:"email,name,first_name,last_name,picture"
              }
            }
          },
          responseInfoCallback);
          new GraphRequestManager()
          .addRequest(infoRequest)
          .start()
        })

    }
  },function(error){
    alert("An error occured"+error.toString());
  }
  )
}

sendsocialinfo(ssoid, email, name, socialtype){
  var formData= new FormData();
  formData.append('sso_id', ssoid)
  formData.append('email', email);
  formData.append('name', name);
  formData.append('login_by', socialtype)
  console.log(formData);
  fetch(urldetails.base_url+"user",{
    method:"POST", body:formData
  })
  .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata.data[0])
    var userdetails={
      userid:jsondata.data[0]["user_id"],
      name:jsondata.data[0]["name"],
      mobile:'',
      email:jsondata.data[0]["email"]
    }
    AsyncStorage.setItem('user',JSON.stringify(userdetails));
    this.props.navigation.navigate("Home")
    this.setState({ spinner: true })
  }).catch((error)=>{
    console.log(error);
    this.setState({ spinner: false })
  })
}



continue(){
  var userdetails={
    mobile:this.state.mobile
  }
  AsyncStorage.setItem('user',JSON.stringify(userdetails));
  this.props.navigation.navigate("Home")
}
  render() {
    return (
      <Container>
      <StatusBar backgroundColor="#9a1922" barStyle="light-content"/>
      <Content style={{backgroundColor:'#263238'}}>
      <LinearGradient colors={['#fff','#fff']}>
      <View style={{alignItems:'center',height:screenHeight-15,paddingTop:'30%'}}>
      
      <Image source={require('../assets/logo.png')} resizeMode="contain" style={{ height: 110,paddingTop:30 }}/>
      <Text style={{textAlign:'center',color:'#dc2430', marginTop:20, fontSize:20}}>Welcome To Chilika Lake !</Text>

        <TouchableOpacity  onPress={()=>this._fbLogin()}  style={{flexDirection:'row',width: screenwidth-40,height: 50,backgroundColor:'#3b5998',borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
                alignContent:'center',alignItems:'center',justifyContent:'center',marginTop:80,elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}>
                <Icon name='logo-facebook'style={{ color: "white", fontSize: 30}}/>
            <Text style={{paddingLeft:10,color: "white", fontSize: 20 }}>Continue with Facebook</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.googlesignIn} style={{flexDirection:'row',width: screenwidth-40,height: 50,backgroundColor:'#ea4335',borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
                alignContent:'center',alignItems:'center',justifyContent:'center',marginTop:10,elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}>
                <Icon name='logo-googleplus'style={{ color: "white", fontSize: 30}}/>
            <Text style={{paddingLeft:10,color: "white", fontSize: 20 }}>Continue with Google</Text>
          </TouchableOpacity>

          <View  style={{ alignItems: 'center', justifyContent: 'center',position:'absolute',bottom:15 }}>
            <Text style={{ color: '#000', fontSize: 10 }}>Powered by A Rabinnson Company</Text>
          </View>
        
        </View>
        </LinearGradient>
      </Content>
      <Modal
            offset={0}
            open={this.state.open}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='red' />
              </View>
            </Modal>
     </Container>
  );
}
}
