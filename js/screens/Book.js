import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions, StatusBar, 
AsyncStorage, Alert, BackHandler } from 'react-native';
import { Container, Header, Left, Body, Right, Icon, Title, Footer, FooterTab, 
  Content, List, ListItem, Item, Input, Spinner, Card } from 'native-base';
import Modal from "react-native-simple-modal";
import Modal2 from 'react-native-simple-modal';
import Modal3 from 'react-native-simple-modal';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import DatePicker from 'react-native-datepicker'
import urldetails from '../config/endpoints.json';
export default class Home extends Component {
  state={
    packagedetails: this.props.navigation.state.params.packagedetails,
    open: false, open2:false, open3:false,
    name:'', gender:'Male', age:'', data:[],
    boatcat:[], boat_id:'', category_name:'',
    cost_add_on_person:'', min_fare_applicable:'',
    minimum_fare:'', number_of_boats:'', user_id:'',
    mobile:'', number_of_person:'', capacity:'',
    total_billing_amount:0, chosenDate: new Date(),
    date:"", available_boats:0, spinner:false
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }

  componentWillMount() {
    this.setState({open2:true})
    AsyncStorage.getItem('user')
    .then((value) => {
        if (value != null || value != undefined) {
            const values = JSON.parse(value)
            console.log('user available' + values.userid)
            this.setState({ mobile: values.mobile, user_id: values.userid })
        }
    })
  }
  
  //modal for loading------------
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });
  
  //modal for selectDate------------
  modalDidOpen2 = () => console.log("Modal did open");
  modalDidClose2 = () => {
    this.setState({ open2: false });
    console.log("Modal did close.");
  };
  openModal2 = () => this.setState({ open2: true });
  closeModal2 = () => this.setState({ open2: false });
  //-

  //modal for adding person------------
  modalDidOpen3 = () => console.log("Modal did open");
  modalDidClose3 = () => {
    this.setState({ open3: false });
    console.log("Modal did close.");
  };
  openModal3 = () => this.setState({ open3: true });
  closeModal3 = () => this.setState({ open3: false });
  //-
  
  save(){
    if(this.state.data.length < this.state.packagedetails.capacity)
    {
      this.state.data.push({"id":this.state.data.length+1, "name":this.state.name,"age":this.state.age,"gender":this.state.gender});
      this.setState({number_of_person:this.state.data.length})
      if(this.state.data.length>0)
      {
        // if(this.state.data.length <= this.state.capacity){
          if(this.state.data.length <= this.state.packagedetails.min_fare_applicable)
          {
            this.setState({total_billing_amount:this.state.packagedetails.minimum_fare})
            console.log(this.state.total_billing_amount)
          }
          else
          {
            this.setState({total_billing_amount:parseInt(this.state.packagedetails.minimum_fare)+parseInt(this.state.packagedetails.cost_add_on_person*(this.state.data.length-this.state.packagedetails.min_fare_applicable))})
            console.log(this.state.total_billing_amount)
          }
        }
        this.setState({open3:false})
      }
      else{
        Alert.alert(
          'Warning',
          "The Capacity of this Boat is "+this.state.packagedetails.capacity+". You can't add more person for this ride.",
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
          )
        }
      }
  
      remove(pos)
      {
        this.state.data.splice(pos, 1);
        console.log(this.state.data)
        this.setState({data:this.state.data})
        if(this.state.data.length>0)
        {
          if(this.state.data.length <= this.state.packagedetails.capacity)
          {
            if(this.state.data.length <= this.state.packagedetails.min_fare_applicable)
            {
              this.setState({total_billing_amount:this.state.packagedetails.minimum_fare})
              console.log(this.state.total_billing_amount)
            }
            else
            {
              this.setState({total_billing_amount:parseInt(this.state.packagedetails.minimum_fare)+parseInt(this.state.packagedetails.cost_add_on_person*(this.state.data.length-this.state.packagedetails.min_fare_applicable))})
              console.log(this.state.total_billing_amount)
            }
          }
        }
      }
      
      checkavailable(){
        this.setState({open:false,spinner:true})
        var formData = new FormData();
        formData.append('package_id', this.state.packagedetails.package_id);
        formData.append('ride_date', this.state.date);
        console.log(formData)
        fetch(urldetails.base_url + urldetails.url["availbooking"],{
          method:"POST",
          body:formData
        }).then((response)=>response.json())
        .then((jsondata)=>{
          console.log(jsondata)
          if(jsondata.status=="success")
          {
            console.log(jsondata.meaasge)
            this.setState({spinner:false,open2:false})
          }
          else
          {
            this.setState({spinner:false})
            Alert.alert(
              'Alert',
              jsondata.msg,
              [
                {text: 'OK', onPress: () =>this.setState({open2:true,})},
              ],
              { cancelable: false }
              )
            }
          }).catch((error) => {
            console.log(error)
            Alert.alert(
              'Alert',
              "Please check your Internet Connection.",
              [
                {text: 'Retry', onPress: () =>this.checkavailable()},
              ],
              { cancelable: false }
              )
            });
          }

          gotoordersummary(){
            if(this.state.date==""||this.state.date=="undefined")
            {
              Alert.alert("Please select the date")
            }
            else{
              console.log(this.state.data.length)
              if(this.state.data.length>0){
                console.log(this.state.data.length)
                this.props.navigation.navigate('Ordersummary',{packagedetails:this.state.packagedetails,
                  data:this.state.data,boat_id:this.state.boat_id, mobile:this.state.mobile,
                  total_billing_amount:this.state.total_billing_amount, date_of_ride:this.state.date, user_id:this.state.user_id })
                }
                else{
                  Alert.alert(
                    'Alert',
                    "Please add person for the ride to continue.",
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                    )
                  }
                }
              }
    
              handle_gender(gender){
                this.setState({gender:gender})
              }

     render() {
       return (
         <Container>
           <Header style={{ backgroundColor: '#df3a45'}} >
             <Left>
               <TouchableOpacity onPress={()=>this.onBackPress()}>
                 <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
                </TouchableOpacity>
              </Left>
              <Body><Title style={{color:'#fff'}}>Booking</Title></Body>
              <Right/>
            </Header>
            <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
            {!this.state.open2  ?
              <View style={{alignContent:'center', justifyContent:'center', marginLeft:3}}>
                <View style={{width:screenwidth-10,backgroundColor:'#fff'}}>
                  <View style={{flexDirection:'row', height:40, backgroundColor:'#EBEBEB', alignContent:'center', alignItems:'center'}}>
                    <TouchableOpacity style={{flexDirection:'row', paddingLeft:2, alignContent:'center', justifyContent:'center'}} onPress={()=>this.setState({open2:true})} >
                      <Left>
                        <Text style={{color:'#000', paddingLeft:5 }}>Date of Ride : </Text>
                      </Left>
                      <Right style={{flexDirection:'row'}}>
                        <Text style={{ fontSize:15, color:'#000', paddingLeft:5}}>{this.state.date}</Text>
                        <FontAwesome style={{ color: '#000', paddingLeft: 5 }} name="pencil-square-o"/>
                      </Right>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection:'row', backgroundColor:'#fff', height:40}}>
                    <Left style={{flexDirection:'row'}}>
                      <Text style={{ color:'#000', paddingLeft:5}}>Package : </Text>
                      <Text style={{ color:'#000', paddingLeft:5 }}>{this.state.packagedetails.package_name}</Text>
                    </Left>
                    <Right style={{flexDirection:'row'}}>
                      <Text style={{ color:'#000'}}>Boat capacity : </Text>
                      <Text style={{ color:'#000', paddingLeft:5 }}>{this.state.packagedetails.capacity}</Text>
                    </Right>
                  </View>
                  <View style={{backgroundColor:'#fff', alignItems:"center", justifyContent:"center"}}>
                    <TouchableOpacity onPress={() => this.setState({open3:true})} style={{ backgroundColor: '#fff', borderColor:'#dc2430', borderWidth:0.2, borderRadius:5}}>
                      <Text style={{ color: '#df3a45', padding: 3, fontSize: 18 }}>+ Add Person to Boat</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
                :
              <View/>
                  }
              
              <Content style={{padding:3}}>
                {/* <View style={{backgroundColor:'#fff'}}>

                </View> */}


                {this.state.data.length>0 ?
                    <Card style={{backgroundColor:'#fff',padding:10,width:screenwidth-10}}>
                      <List  style={{marginTop:10}}>
                        {
                          this.state.data.map((data, i) => {
                            return(
                              <ListItem style={{marginRight:10}} key={i}>
                                <Body>
                                <Text style={{ fontSize:12, color:'#000' }}>S.No. : {i+1}</Text>
                                  <Text style={{ fontSize:12, color:'#000'  }}>Name : {data.name}</Text>
                                  <Text style={{ fontSize:12, color:'#000' }}>Age : {data.age}</Text>
                                  <Text style={{ fontSize:12, color:'#000' }}>Gender : {data.gender}</Text>
                                </Body>
                                <Right>
                                  <Icon onPress={()=>this.remove(i)} name='md-trash' style={{ color: "red", fontSize: 25 }} />
                                </Right>
                              </ListItem>
                              )}
                              )}
                        </List>
                      </Card>
                      :
                      <View/>}
                </Content>
                
                {this.state.data.length>0 ?
                  <View style={{ backgroundColor: '#F4F6F7',height:50,paddingBottom:10,alignContent:'center',alignItems:'center' }}>
                    <View style={{flexDirection:'row'}}>
                      <Left>
                        <Text style={{color:'#000',padding:10}}>Grand Total:</Text>
                      </Left>
                      <Body/>
                      <Right>
                        <Text style={{color:'#000',padding:10}}>₹ {this.state.total_billing_amount}</Text>
                      </Right>
                    </View>
                  </View>:<View/>}
              
              <Footer>
                <FooterTab style={{ backgroundColor: '#fff'}}>
                  <LinearGradient colors={['#7b4397', '#dc2430']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center"}}>
                    <TouchableOpacity onPress={() => this.gotoordersummary()} style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}>
                      <Text style={{ color: '#fff',textAlign:'center'}}>CONTINUE</Text>
                      <Icon style={{ color: '#fff',  alignItems:'center', paddingLeft:10 }} name='ios-arrow-round-forward' />
                      </TouchableOpacity>
                    </LinearGradient>
                  </FooterTab>
                </Footer>

            <Modal offset={0} open={this.state.open} closeOnTouchOutside={false} disableOnBackPress={true} 
            modalDidOpen={this.modalDidOpen} modalDidClose={this.modalDidClose} 
            style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
              <View style={{alignContent:'center',alignItems:'center'}}>
                <Spinner color='red' />
              </View>
            </Modal>

          <Modal2 offset={0} closeOnTouchOutside={false} disableOnBackPress={true}
            open={this.state.open2} modalDidOpen={this.modalDidOpen2} modalDidClose={this.modalDidClose2}
            style={{ color: "#F5F5F5" }}>
              <View style={{alignContent:'center',alignItems:'center',height:screenHeight/3}}>
                <Text style={{fontSize:20}}>Select Date of Ride</Text>
                <View style={{paddingTop:30,alignContent:'center',alignItems:'center'}}>
                    <DatePicker style={{width:screenwidth-100,borderColor:'#d9d9d9',borderWidth:0.5}}
                    date={this.state.date} showIcon={false} mode="date" placeholder="select date" 
                    placeholderTextColor="#fff" format="YYYY-MM-DD" minDate={new Date()} maxDate="3000-01-01"
                    confirmBtnText="Confirm" cancelBtnText="Cancel" 
                    customStyles={{ dateIcon: { position: 'absolute', left: 0, top: 4, marginLeft: 0 },
                    dateInput: { placeholderTextColor:'#000' }}}
                    onDateChange={(date) => {this.setState({date: date})}}/>
                    
                  <View style={{paddingTop:30}}>
                    <TouchableOpacity  onPress={() => this.checkavailable()} style={{backgroundColor:'#df3a45' ,width: screenwidth-100,height: 50,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
                      <Text style={{ color: "#fff", fontSize: 20 }}>Submit</Text>
                    </TouchableOpacity>
                  </View>
                  {this.state.spinner?<Spinner color='red'/>:<View/>}
                </View>
              </View>
          </Modal2>

          <Modal3 offset={0} closeOnTouchOutside={false} disableOnBackPress={true}
            open={this.state.open3} modalDidOpen={this.modalDidOpen3} 
            modalDidClose={this.modalDidClose3} style={{alignItems: "center"}}>
              <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
                <Left/>
                  <Text style={{ fontSize: 25, marginBottom: 10, textAlign: 'center' }}>Add Person</Text>
                <Right>
                  <TouchableOpacity style={{ borderRadius: 25, width: 25, height: 25,marginBottom: 20, }} onPress={() => this.setState({ open3: false })} >
                      <Text style={{ color: '#595959',fontSize:25,  padding: 2, textAlign: 'center' }}>X</Text>
                    </TouchableOpacity>
                  </Right>
                </View>

                <View style={{flexDirection:'row'}}>
                
                {this.state.gender=="Male"?
                <TouchableOpacity onPress={()=>this.handle_gender("Male")}
                style={{alignItems:'center', width:screenwidth/4, height:60,backgroundColor:'green', marginLeft:5, borderColor:'#fff', borderWidth:1}}>
                  <Icon name="md-male" style={{ color: '#fff', padding:5 }} />
                  <Text style={{color:'#fff'}}>Male</Text>
                </TouchableOpacity>:
                  <TouchableOpacity onPress={()=>this.handle_gender("Male")}
                  style={{alignItems:'center', width:screenwidth/4, height:60, marginLeft:5, borderColor:'#000', borderWidth:1}}>
                    <Icon name="md-male" style={{ color: '#b3b3b3', padding:5 }} />
                    <Text>Male</Text>
                  </TouchableOpacity>}

                  {this.state.gender=="Female"?
                <TouchableOpacity onPress={()=>this.handle_gender("Female")}
                style={{alignItems:'center', width:screenwidth/4, height:60,backgroundColor:'green', marginLeft:5, borderColor:'#fff', borderWidth:1}}>
                  <Icon name="md-female" style={{ color: '#fff', padding:5 }} />
                  <Text style={{color:'#fff'}}>Female</Text>
                </TouchableOpacity>:
                  <TouchableOpacity onPress={()=>this.handle_gender("Female")}
                  style={{alignItems:'center', width:screenwidth/4, height:60, marginLeft:5, borderColor:'#000', borderWidth:1}}>
                    <Icon name="md-female" style={{ color: '#b3b3b3', padding:5 }} />
                    <Text>Female</Text>
                  </TouchableOpacity>}

                  {this.state.gender=="Transgender"?
                <TouchableOpacity onPress={()=>this.handle_gender("Transgender")}
                style={{alignItems:'center', width:screenwidth/4, height:60,backgroundColor:'green', marginLeft:5, borderColor:'#fff', borderWidth:1}}>
                  <Icon type="FontAwesome" name="transgender" style={{ color: '#fff', padding:5 }} />
                  <Text style={{color:'#fff'}}>Transgender</Text>
                </TouchableOpacity>:
                  <TouchableOpacity onPress={()=>this.handle_gender("Transgender")}
                  style={{alignItems:'center', width:screenwidth/4, height:60, marginLeft:5, borderColor:'#000', borderWidth:1}}>
                    <Icon type="FontAwesome" name="transgender" style={{ color: '#b3b3b3', padding:5 }} />
                    <Text>Transgender</Text>
                  </TouchableOpacity>}
                  
                </View>
              
              <View style={{alignItems:'center', justifyContent: 'center', marginTop:20 }}>
                <Item regular style={{ width: screenwidth - 80, alignItems: 'center', backgroundColor: '#f7f7f7', borderRadius: 40, marginTop: 10, }}>
                  <Icon type="Entypo" name='user' style={{ color: '#b3b3b3' }} />
                  <Input style={{ fontSize: 16, color: 'black' }} placeholder='Name' placeholderTextColor='#9a1922' autoCorrect={false} autoCapitalize="none" onChangeText={(name) => this.setState({ name: name })} />
                </Item>
                
                <Item regular style={{ width: screenwidth - 80, alignItems: 'center', backgroundColor: '#f7f7f7', borderRadius: 40, marginTop: 10, }}>
                  <Icon type="AntDesign" name='calendar' style={{ color: '#b3b3b3' }} />
                  <Input style={{ fontSize: 16, color: 'black' }} placeholder='Age' placeholderTextColor='#9a1922' autoCorrect={false} autoCapitalize="none" onChangeText={(age) => this.setState({ age: age })}  keyboardType={'phone-pad'} />
                </Item>
                
                <TouchableOpacity onPress={() => this.save()}>
                <LinearGradient  colors={['#7b4397', '#dc2430']} start={{ x: 1, y: 0.1 }} end={{ x: 0.0, y: 0.25 }} style={{ width: screenwidth - 80, height: 50, borderRadius: 40, alignItems: "center", justifyContent: "center", marginTop: 45, marginBottom: 20 }}>
                  <TouchableOpacity onPress={() => this.save()} style={{ marginRight: 5, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ color: "#fff", fontSize: 20 }}>ADD</Text>
                  </TouchableOpacity>
                </LinearGradient>
                </TouchableOpacity> 
              </View>

        </Modal3>
        </Container>
    );
}
}


