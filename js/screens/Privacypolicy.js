

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
  import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator, Badge,
    Item, Input, Spinner, Card, CardItem,
  } from 'native-base';
  import LinearGradient from 'react-native-linear-gradient';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class Privacypolicy extends Component {
    state = {
        spinner: true,
      };
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Home");
        return true;
      };
  render() {
    return (
    <Container>
          <Header style={{ backgroundColor:'#df3a45' }} >
          <Left>
          <Icon name='arrow-back' onPress={()=>this.props.navigation.navigate("Home")}
             style={{ fontSize: 25, color: '#fff' }} />
          </Left>
          <Body><Title>Privacy Policy</Title></Body>
        </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
     <Content>
   <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
  <Text style={{fontSize:20,padding:10,alignContent:'center',justifyContent:'center'}}>
  At Babydolphin, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by Babydolphin and how we use it. If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us at our contact page.
</Text>
<Text style={{fontSize:20,padding:10}}>
we recognize the importance of protecting the personal information collected from users in the operation of its services and taking reasonable steps to maintain the security, integrity and privacy of any information in accordance with this Privacy Policy.
</Text>
<Text style={{fontSize:20,padding:10}}>
By using our website, you hereby consent to our Privacy Policy and agree to its Terms and Conditions.
</Text>
   </View> 
   </Content> 
    </Container>
  );
}
}



 

