import React, { Component } from 'react';
import {  View, Text, TouchableOpacity, Dimensions, StatusBar, AsyncStorage, Alert, ImageBackground, BackHandler,Image,Linking} from 'react-native';
import { Container,Content, Header, Left, Body, Right,Icon, Thumbnail,Item, Input,Spinner} from 'native-base';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import FBSDK,{LoginManager,AccessToken,GraphRequest,GraphRequestManager} from 'react-native-fbsdk';
export default class Select extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    BackHandler.exitApp()
  // this.props.navigation.navigate("Select")
  //   return true;
  };

  _fbLogin(){
    this.setState({ spinner: true })
    LoginManager.logInWithReadPermissions(['public_profile','email'])
    .then((result)=>{
      if(result.isCancelled){
        alert("Login is cancelled "+result.isCancelled)
        this.setState({ spinner: false })
      }else{
          AccessToken.getCurrentAccessToken()
          .then((data)=>{
            let accessToken = data.accessToken
            this.setState({name:data.accessToken.toString()})

            const responseInfoCallback = (error,result)=>{
              if(error){
                console.log(error)
                this.setState({ spinner: false })
                //alert("Error fetching data : "+error.toString())

              }else{
                alert(result.name)
                // this.sendsocialinfo(result.id,result.email,result.name,"Facebook")
              }
            }
            const infoRequest = new GraphRequest('/me',{
              accessToken:accessToken,
              parameters:{
                fields:{
                  string:"email,name,first_name,last_name,picture"
                }
              }
            },
            responseInfoCallback);
            new GraphRequestManager()
            .addRequest(infoRequest)
            .start()
          })

      }
    },function(error){
      alert("An error occured"+error.toString());
    }
    )
}


  render() {
    return (
      <Container>
         <Content style={{backgroundColor:'#263238'}}>
         <LinearGradient colors={['#dc2430','#7b4397']}>
          <StatusBar backgroundColor="#000" barStyle="light-content"/>
            <View style={{alignItems:'center',marginTop:10,height:screenHeight-20}}>
              <Image source={require('../assets/logo.png')}resizeMode="contain" style={{ width:screenwidth/2,height: 200,marginTop:10 }}/>
              <Text style={{fontSize:35,color:'#fff'}}>Baby Dolphin</Text>
              <View style={{marginTop:'70%',justifyContent: 'center',alignItems: 'center'}}>
              <TouchableOpacity  onPress={()=>this.props.navigation.navigate("Login")}  style={{flexDirection:'row',width: screenwidth-40,height: 50,backgroundColor:'#E91E63',borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
                marginTop:10,elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}>
                <Icon name='ios-mail'style={{ color: "white", fontSize: 22,paddingRight:5,paddingLeft:100,paddingTop:15 }} />
            <Text style={{ color: "white", fontSize: 20,paddingTop:15 }}>Continue with Email</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>this._fbLogin()}    style={{flexDirection:'row',width: screenwidth-40,height: 50,backgroundColor:'#3b5998',borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
  marginTop:10,alignItems: "center",justifyContent: 'center',elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}} >

            <Icon name='logo-facebook'style={{ color: "white", fontSize: 20,paddingRight:5,paddingLeft:100,paddingTop:15 }} />
            <Text style={{ color: "white", fontSize: 20,paddingTop:15 }}>Continue with Facebook</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity    style={{flexDirection:'row',width: screenwidth-40,height: 50,backgroundColor:'#B22222',borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
  marginTop:10,elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}>
            <Icon name='logo-google'style={{ color: "white", fontSize: 20,paddingRight:5,paddingLeft:100,paddingTop:15 }} />
            <Text style={{ color: "white", fontSize: 20,paddingTop:15 }}>Continue with Google</Text>
          </TouchableOpacity> */}
          </View>
          <View  style={{ flex: 0.2, alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity onPress={()=>{Linking.openURL('https://www.epsumlabs.com/')}}>
              <Text style={{ color: 'white', fontSize: 10 }}>Powered by Epsum Labs Pvt. Ltd</Text>
            </TouchableOpacity> 
          </View>
        </View> 
      
      </LinearGradient>
      </Content>
     
     </Container>
  );
}
}
