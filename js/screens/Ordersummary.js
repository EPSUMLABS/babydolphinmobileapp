import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, StatusBar, 
AsyncStorage, Alert, ImageBackground,BackHandler,ScrollView,Picker,TextInput } from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,Badge,
    Item, Input, Spinner,Card, CardItem
} from 'native-base';
import Modal from "react-native-simple-modal";
import LinearGradient from 'react-native-linear-gradient';
import RazorpayCheckout from 'react-native-razorpay';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import DatePicker from 'react-native-datepicker'
import urldetails from '../config/endpoints.json';
export default class Ordersummary extends Component {
    state = {
      open:false,
      packagedetails: this.props.navigation.state.params.packagedetails,
        data: this.props.navigation.state.params.data,
        package_id:this.props.navigation.state.params.package_id,
        mobile:this.props.navigation.state.params.mobile,
        user_id:this.props.navigation.state.params.user_id,
        total_billing_amount:this.props.navigation.state.params.total_billing_amount,
        date_of_ride:this.props.navigation.state.params.date_of_ride,
        name:''
    }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Book");
    return true;
  };
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
componentWillMount(){
    console.log(this.state.data)
}
modalDidOpen = () => console.log("Modal did open.");
modalDidClose = () => {
  this.setState({ open: false });
  console.log("Modal did close.");
};
moveUp = () => this.setState({ offset: -100 });
resetPosition = () => this.setState({ offset: 0 });
openModal = () => this.setState({ open: true });
closeModal = () => this.setState({ open: false });
booking(){
var date = this.state.date_of_ride;
var ride_date = date.split("-").reverse().join("-");
if(this.state.name==undefined||this.state.name=="")
{
  alert("Please Enter Your Name")
}
else if(this.state.mobile=="" || this.state.mobile==undefined)
{
  alert("Please enter your mobile number")
}else{
  this.setState({open:true})
  var options = {
    description: 'Credits towards Boat Booking',
    image: 'https://i.imgur.com/3g7nmJC.png',
    currency: 'INR',
    key: 'rzp_live_wNXjkXHScEIGGP',
    amount:this.state.total_billing_amount+'00',
    external: {
      wallets: ['paytm']
    },
    name:this.state.name,
    prefill: {
      contact:this.state.mobile,
      name:this.state.name
    },
    theme: {color: '#F37254'}
  }
  RazorpayCheckout.open(options).then((data) => {

    
  var formData= new FormData();
  formData.append('package_id',this.state.packagedetails.package_id);
  formData.append('booker_name',this.state.name);
  formData.append('contact',this.state.mobile);
  formData.append('person_data',JSON.stringify(this.state.data));
  formData.append('amount',this.state.total_billing_amount);
  formData.append('mode_of_pay',"ONLINE");
  formData.append('pay_status',"PAID");
  formData.append('book_via',"Mobile APP");
  formData.append('boat_capacity',this.state.packagedetails.capacity)
  formData.append('ride_date',this.state.date_of_ride);
  //formData.append('txnid',data.razorpay_payment_id);
  formData.append('txnid','abctest');
  formData.append('user_id',this.state.user_id);
  console.log(formData)
  fetch(urldetails.base_url + urldetails.url["booking"],{
    method:"POST",
    body:formData
  }) .then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
      this.setState({open:false})
      console.log(jsondata.data[0])
      this.props.navigation.navigate("Viewbooking")
    }
    else{
      Alert.alert(
        'Alert',
        jsondata.msg,
        [
          {text: 'OK', onPress: () => this.props.navigation.navigate(("Book"),
          {packagedetails:this.state.packagedetails})},
        ],
        { cancelable: false }
      )
      this.setState({open:false})
    }
  })
  .catch((error) => {
    console.log(error)
    Alert.alert(
      'Alert',
      "Please check your Internet Connection .",
      [
        {text: 'Retry', onPress: () => this.booking()},
      ],
      { cancelable: false }
    )
  });
  }).catch((error) => {
    // handle failure
    alert(`Error: ${error.code} | ${error.description}`);
  });
  RazorpayCheckout.onExternalWalletSelection(data => {
    alert(`External Wallet Selected: ${data.external_wallet} `);
  });
}
}
remove(name,category){
    this.state.data.pop( {name,category} );
    console.log(this.state.data)
    this.setState({data:this.state.data})
    if(this.state.data.length>0){
      if(this.state.data.length <= this.state.capacity){
      if(this.state.data.length <= this.state.min_fare_applicable){
        this.setState({total_billing_amount:this.state.minimum_fare})
        console.log(this.state.total_billing_amount)
      }
      else{
        this.setState({total_billing_amount:parseInt(this.state.minimum_fare)+parseInt(this.state.cost_add_on_person*(this.state.data.length-this.state.min_fare_applicable))})
     console.log(this.state.total_billing_amount)
      }
    }
  }
  }
     render() {
   
        return (
        <Container>
                <Header  style={{ backgroundColor: '#df3a45', alignContent:'center',justifyContent:"center"}}>
                    <Left>
                      <TouchableOpacity onPress={()=>this.props.navigation.navigate("Book")}>
                            <Icon name='arrow-back' style={{ fontSize: 25, color: '#fff' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                    <Title style={{color:'#fff',textAlign:'center'}}>Summary</Title>
                    </Body>
                    <Right/>
                </Header>
                <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
                <Content style={{backgroundColor:'#FDFEFE'}}>
                <View style={{alignContent:'center',alignItems:'center'}}> 
               <Card style={{width:screenwidth-20,}}>
               <CardItem style={{flexDirection:'row',backgroundColor:'#EBEBEB',alignContent:'center',alignItems:'center'}}>
                <TouchableOpacity style={{flexDirection:'row',paddingLeft:2, alignContent:'center', justifyContent:'center'}} onPress={()=>this.setState({open2:true})} >
               <Left>
                <Text style={{color:'#000',paddingRight:5 }}>Date of Ride : </Text>
                </Left>
                <Right style={{flexDirection:'row'}}>
                <Text style={{ fontSize:15, color:'#000', paddingLeft:5}}>{this.state.date_of_ride}</Text>
                </Right>
               </TouchableOpacity>
                  </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>No. Of Riders : </Text>
                            <Text style={{ fontSize: 16 }}>{this.state.data.length}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>Package Name : </Text>
                            <Text style={{ fontSize: 16 }}>{this.state.packagedetails.package_name}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>Billing Amount : </Text>
                            <Text>{this.state.total_billing_amount}</Text>
                        </CardItem>
                  </Card> 
                  {/* <View style={{paddingTop:5}}>
                  <Item regular style={{width:screenwidth-20,backgroundColor:'#fff',borderColor:'#000',borderWidth:0.5,borderRadius:2}}>
            <Input style={{fontSize:16}} placeholder='Enter your Name' placeholderTextColor='#000'  onChangeText={(name)=>this.setState({name:name})} autoCorrect={false}/>
          </Item>
          </View> */}
           <View style={{paddingTop:5}}>
          <View style={{flexDirection:'row',backgroundColor:'#EBEBEB',width:screenwidth-20,height:screenHeight/17,borderRadius:10,alignContent:'center',alignItems:'center'}}>
          <Icon style={{  fontSize: 20,paddingLeft:10 }} name='md-person' />
          <Input style={{fontSize:16,paddingLeft:10}} placeholder='Enter your Name' placeholderTextColor='#000'  onChangeText={(name)=>this.setState({name:name})} autoCorrect={false}/>
            </View>

            <View style={{flexDirection:'row',backgroundColor:'#EBEBEB',width:screenwidth-20,height:screenHeight/17,borderRadius:10,alignContent:'center',alignItems:'center', marginTop:10}}>
          <Icon style={{  fontSize: 20,paddingLeft:10 }} type="Entypo" name='mobile' />
          <Input style={{fontSize:16,paddingLeft:10}} placeholder='Enter your Mobile' placeholderTextColor='#000'  onChangeText={(mobile)=>this.setState({mobile:mobile})} keyboardType={"phone-pad"} autoCorrect={false}/>
            </View>
             </View>
            </View>
        </Content> 
        <Modal
            offset={0}
            open={this.state.open}
            closeOnTouchOutside={false} disableOnBackPress={true}
            modalDidOpen={this.modalDidOpen}
            modalDidClose={this.modalDidClose}
            style={{ alignItems: "center", backgroundColor: "#000", width:screenwidth }}>
            <View style={{alignContent:'center',alignItems:'center'}}>
            <Spinner color='red' />
              </View>
            </Modal>
        <Footer>
            <FooterTab>
         


          <LinearGradient colors={['#7b4397', '#dc2430']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center"}}>
            <TouchableOpacity onPress={() => this.booking()} style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}>
                <Text style={{ color: '#fff',textAlign:'center'}}>PROCCED TO PAYMENT</Text>
                <Icon style={{ color: '#fff',  alignItems:'center', paddingLeft:10 }} name='ios-arrow-round-forward' />
            </TouchableOpacity>
            </LinearGradient>
         
            </FooterTab>
          </Footer>
         
    
        </Container>
    );
}
}


