import React, { Component } from 'react';
import {  View, Text, TouchableOpacity, Dimensions, StatusBar, AsyncStorage, ImageBackground, BackHandler, Image, Alert} from 'react-native';
import { Container,Content, Header,Title,Left, Body, Right, Icon, Footer, 
  FooterTab, Thumbnail, Item, Input, Spinner} from 'native-base';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import Imagel from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
import Modal from "react-native-simple-modal";
import urldetails from '../config/endpoints.json';


export default class Packages extends Component{
  state={
    open:true,
    packagedetails:[]
  }

  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
    this.setState({ open: false });
  };
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }


  componentWillMount(){
    fetch(urldetails.base_url + urldetails.url["package"],{
        method:"GET"
      }) .then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)
        if(jsondata.status=="success"){
         
           this.setState({open:false,packagedetails:jsondata.data})
        }
       
      })  .catch((error) => {
        console.log(error)
        Alert.alert(
          'Alert',
          "Please check your Internet Connection.",
         
          { cancelable: false }
        )
      });
}


gotodescriptionpage(data){
    this.setState({open1:false})
    this.props.navigation.navigate(("descriptionpage"),{packagedetails:data})
  }

render() {
    return (
        <Container>
            <Header style={{ backgroundColor: '#df3a45' }}>
                <Left>
                    <TouchableOpacity transparent onPress={() => this.props.navigation.openDrawer()}>
                        <Icon type="AntDesign" name='bars' style={{ fontSize: 25, color: '#fff' }} />
                    </TouchableOpacity>
                </Left>
                <Body><Title>Holiday Packages</Title></Body>
               
            </Header>
            <StatusBar backgroundColor="#df3a45" barStyle="light-content" />
            <Content>
            <View style={{ alignContent: 'center', alignItems: 'center', backgroundColor:'#e6e6e6' }}>
                    <View>
                    {
                          this.state.packagedetails.map((data, i) => {
                            return(
                    <View key={{i}} style={{borderColor:'#fff', borderWidth:0.5, marginTop: 5, backgroundColor:'#fff'}}>
                       <Text style={{ textAlign:'center', padding: 5, fontSize:18,color:'#000' }}>{data.package_name}</Text>
                      <TouchableOpacity style={{ justifyContent: "center", alignContent: 'center', alignItems: 'center', paddingTop: 3 }} 
                      onPress={() =>this.gotodescriptionpage(data)}>
                        <Imagel square source={{ uri: urldetails.image+ data.package_image}} style={{ height: (Dimensions.get("window").height / 4), width: (Dimensions.get("window").width) - 5 }} 
                        indicator={ProgressBar.Pie}
                        indicatorProps={{ size: 80, borderWidth: 0, color: 'rgba(150, 150, 150, 1)', unfilledColor: 'rgba(200, 200, 200, 0.2)' }}/>
                      </TouchableOpacity>
                     
                      <Text style={{ textAlign:'center', padding: 5, fontSize:14 }}>{data.description.substr(0,50)+"..."}</Text>
                    </View>
                           ) })
                        }
                        </View>
                    
          </View>
            </Content>
            <Modal
          offset={0}
          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          style={{ alignItems: "center", backgroundColor: "#000", width: screenwidth }}>
          <View style={{ alignContent: 'center', alignItems: 'center' }}>
            <Spinner color='red' />
          </View>
        </Modal>
        </Container>
        );
    }
}
