

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
  import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator, Badge,
    Item, Input, Spinner, Card, CardItem,Textarea,Fab
  } from 'native-base';
  import LinearGradient from 'react-native-linear-gradient';
  import urldetails from '../config/endpoints.json';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import StarRating from 'react-native-star-rating';
import Modal1 from 'react-native-simple-modal';
import Fontawesome from 'react-native-vector-icons/FontAwesome';
import Imagel from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
export default class description extends Component {
    state = {
      packagedetails: this.props.navigation.state.params.packagedetails,
        spinner: true,
        starCount: 3,
        review:"",
        name:"",
        mobile:"",
        active:true,
        open1:false,
        data:[]
      };
      componentWillMount(){
        AsyncStorage.getItem('user')
        .then((value) => {
            if (value != null || value != undefined) {
                const values = JSON.parse(value)
                console.log('user mobile no' + values.mobile)
                console.log(this.state.packagedetails)
                this.setState({mobile:values.mobile })
            }
          })
          this.viewreview()
      }
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Packages");
        return true;
      };
      review(){
        var formData = new FormData();
        formData.append('review_name', this.state.name);
        formData.append('review_mobno', this.state.mobile);
        formData.append('review_rating', this.state.starCount);
        formData.append('review_msg', this.state.review);
        formData.append('review_package', this.state.packagedetails.package_id);
        console.log(formData)
        fetch(urldetails.base_url + urldetails.url["review"],{
            method:"POST",
            body:formData
          }).then((response)=>response.json())
          .then((jsondata)=>{
            console.log(jsondata)
            if(jsondata.status=="success"){
             Alert.alert('',jsondata.msg)
             this.setState({open1:false})
                            }
                            else{
                                Alert.alert('',jsondata.msg) 

                            }
          })  .catch((error) => {
            console.log(error)
            Alert.alert(
                'Alert',
                "Please check your Internet Connection.",
                [
                  {text: 'Retry', onPress: () =>this.review()},
                ],
                { cancelable: false }
              )
          });
    }
    viewreview(){
      var formData = new FormData();
        formData.append('package_id', this.state.packagedetails.package_id);
      fetch(urldetails.base_url + urldetails.url["viewreview"],{
        method:"POST",
        body:formData
      }).then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)
        if(jsondata.status=="success")
        {
            this.setState({data:jsondata.data,open:false})
            console.log(this.state.data)
        }
      })
    }
    onStarRatingPress(rating) {
      this.setState({
        starCount: rating
      });
    }
  render() {
    return (
    <Container>
          <Header style={{ backgroundColor:'#df3a45'}} >
          <Left>
          <Icon name='arrow-back' onPress={()=>this.props.navigation.navigate("Home")}
             style={{ fontSize: 25, color: '#fff' }} />
          </Left>
          <Body><Title>{this.state.packagedetails.package_name}</Title></Body>
          <Right>
          </Right>
        </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
     <Content>
   <Imagel source={{uri:urldetails.image+ this.state.packagedetails.package_image}} style={{width:screenwidth,height:screenHeight/3}}
   indicator={ProgressBar.Pie}
   indicatorProps={{ size: 80, borderWidth: 0, color: 'rgba(150, 150, 150, 1)', unfilledColor: 'rgba(200, 200, 200, 0.2)' }}/>
    
          {/* <Icon name='arrow-back' style={{ fontSize: 25, color: '#fff',paddingLeft:4, paddingTop:10}} onPress={()=>this.onBackPress()}/> */}
  
<View style={{padding:10}}>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Package Name : </Text>
<Text>{this.state.packagedetails.package_name}</Text>
</View>
<Text/>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Boating Duration : </Text>
<Text>{this.state.packagedetails.duration}</Text>
</View>
<Text/>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Minimum Fare : </Text>
<Text>{this.state.packagedetails.minimum_fare}</Text>
</View>
<Text/>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Capacity of Boat : </Text>
<Text>{this.state.packagedetails.capacity}</Text>
</View>

<Text/>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Facility : </Text>
<Text>{this.state.packagedetails.facility}</Text>
</View>
<Text/>
<View style={{flexDirection:'row'}}>
<Text style={{fontWeight:'bold'}}>Places : </Text>
<Text>{this.state.packagedetails.places}</Text>
</View>
<Text/>
<Text style={{fontWeight:'bold'}}>Package Details : </Text>
<Text>{this.state.packagedetails.description}</Text>
    </View>
    <View style={{padding:10}}>
    
    <View  style={{alignContent:'center',alignItems:'center',}}>
    {this.state.data.length>0?<Text style={{fontWeight:'bold'}}>REVIEWS</Text>:<Text/>}
                {
                  this.state.data.map((data, i) => {
                    return(
                      <Card style={{width:screenwidth-20,backgroundColor:'#F7F9F9',elevation:2}} key={i}>
                     <View style={{padding:10}}>
                     <View style={{flexDirection:'row'}}>
                     <Left>
                       <Text style={{color:'#000'}}>{data.review_name}</Text></Left>
                    <Right>
                      <View style={{width:20,height:20}}>
                    {data.review_rating>3?
                    <Text style={{fontSize:15,color:'#fff',backgroundColor:'#009900',textAlign:'center'}}>{data.review_rating}</Text>:
                    data.review_rating<2?<Text style={{fontSize:15,color:'#fff',backgroundColor:'#b30000',textAlign:'center'}}>{data.review_rating}</Text>:
                    <Text style={{fontSize:15,color:'#fff',backgroundColor:'#ffcc00',textAlign:'center'}}>{data.review_rating}</Text>}
                     
                     </View>
                     </Right>
                     </View>
                          <Text style={{fontSize:10}}>{data.reviewadded_on}</Text>
                     </View>
                      <CardItem>
                          <Text style={{fontSize:14}}>{data.review_msg}</Text>
                      </CardItem>
                </Card>   
                    )}
                  )}
                </View>
    </View>
  
      </Content>
      {/* <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{ }}
            style={{ backgroundColor: '#df3a45',marginBottom:37 }}
            position="bottomRight"
            onPress={() => this.setState({ open1:true,active: !this.state.active })}>
            <Fontawesome name="pencil" />
           
          </Fab> */}
      <Footer>
          <FooterTab style={{ backgroundColor: '#fff' }}>
          <LinearGradient colors={['#7b4397', '#dc2430']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center",flexDirection:'row'}}>
            <TouchableOpacity  onPress={() =>this.props.navigation.navigate(("Book"),
                  {packagedetails:this.state.packagedetails})}style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}>
                <Text style={{ color: '#fff',textAlign:'center'}}>BOOK NOW</Text>
                <Icon style={{ color: '#fff',  alignItems:'center', paddingLeft:10 }} name='ios-arrow-round-forward' />
            </TouchableOpacity>
            </LinearGradient>
          </FooterTab>
        </Footer>
        <Modal1
                    offset={0}
                    open={this.state.open1}
                    closeOnTouchOutside={false} disableOnBackPress={true}
                    modalDidOpen={this.modalDidOpen1}
                    modalDidClose={this.modalDidClose1}
                    style={{ alignItems: "center" }}
                >
                  <View style={{flexDirection:'row'}}>
             <Left>
             </Left>
             <Body>
             <Text style={{fontSize:20,color:'#E91E63'}}>Review</Text>
               </Body>
             <Right>
             <TouchableOpacity style={{ backgroundColor: 'red', borderRadius: 25,   width: 25, height: 25 }} onPress={() => this.setState({ open1: false })} >
                  <Text style={{ color: '#fff', fontWeight: 'bold',marginTop:1, padding: 2, textAlign: 'center' }}>X</Text>
                </TouchableOpacity>
             </Right>
           </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ backgroundColor: 'transparent', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <StarRating
        disabled={false}
        emptyStar={'ios-star-outline'}
        fullStar={'ios-star'}
        halfStar={'ios-star-half'}
        iconSet={'Ionicons'}
        maxStars={5}
        rating={this.state.starCount}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
        fullStarColor={'#FFD700'}
      />                    
                        </View>
              <Item regular style={{width:'95%',alignItems:'center',backgroundColor:'#fff',borderRadius:5,marginTop:20,textAlign:'center',justifyContent:'center'}}>
          <Input style={{fontSize:16,color:'black'}} placeholder='Your Name'  onChangeText={(name)=>this.setState({name:name})} autoCorrect={false} />
        </Item>
                        <Textarea style={{ width: '95%',  }} rowSpan={5} bordered placeholder="Write Your Review Here" onChangeText={(rev) => this.setState({ review: rev })} />
                        {/* {this.state.spinner ? <View><Spinner color='#ffb400' /></View> : <View />} */}
                       
                        <TouchableOpacity onPress={() => this.review()} style={{ width: screenwidth - 75, height: 50,  borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                            backgroundColor:'#df3a45',alignItems: "center", justifyContent: 'center',}} 
                        >
                            <Text style={{ color: "white", fontSize: 20 }}>SUBMIT</Text>

                        </TouchableOpacity>
                       
                       
                        {/* <TouchableOpacity  style={{ width: screenwidth - 75, height: 50,  borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                            alignItems: "center", justifyContent: 'center',backgroundColor:'#fff',elevation:8,borderRadius:5,borderColor:'#c81b2a',borderWidth:0.5 }} onPress={() => this.props.navigation.navigate("Viewreview")} 
                        >
                            <Text style={{ color: "white", fontSize: 20 }}>View Review</Text>

                        </TouchableOpacity> */}
                    </View>
                </Modal1>
      </Container>
  );
}
}



 

