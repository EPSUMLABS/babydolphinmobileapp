

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
  import {
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab
  } from 'native-base';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import LinearGradient from 'react-native-linear-gradient';
export default class Splash extends Component {
    state = {
        spinner: true,
      };

componentWillMount() {
  AsyncStorage.getItem('user')
      .then((value) => {
          if (value != null || value != undefined) {
              const values = JSON.parse(value)
              console.log('user available' + values.userid)
            //   this.setState({ useridval: values.userid, name:values.username,email:values.email,contact:values.contact,password:values.password })
              //this.userProfile()
              setTimeout(() => this.props.navigation.navigate('Home'), 1000);
          }
          else{
            setTimeout(() => this.props.navigation.navigate('Login'), 1000);
          }
      })
}
  render() {
    return (
    <Container>
        <Content>
        <LinearGradient colors={['#dc2430','#7b4397']} style={{ flex: 1, alignItems: 'center',height:screenHeight-15 }}>
          <StatusBar backgroundColor="#9a1922" barStyle="light-content"/>
         
          <View style={{ flex: 0.9, alignItems: 'center', justifyContent: 'center' }}>
              <Image
                  style={{ width: 200, height: 200, resizeMode: 'contain', alignItems: 'center', justifyContent: 'center' }}
                  source={require('../assets/logo.png')}
              />
              <Text style={{fontSize:35,color:'#fff'}}>Baby Dolphin</Text>
              <Spinner color='#fff' />
          </View>
          <View  style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ color: 'white', fontSize: 10 }}>Powered by A Rabinnson Company</Text>
          </View>
      </LinearGradient>
      </Content>
      </Container>
  );
}
}

const styles = StyleSheet.create({

});

 

