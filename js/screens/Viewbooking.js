import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, StatusBar, 
AsyncStorage, Alert, ImageBackground,BackHandler,ScrollView, } from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,Badge,
    Item, Input, Spinner,Card, CardItem,Textarea
} from 'native-base';
import urldetails from '../config/endpoints.json';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import StarRating from 'react-native-star-rating';
import Modal1 from 'react-native-simple-modal';
import Modal2 from 'react-native-simple-modal';

import LinearGradient from 'react-native-linear-gradient';
export default class Viewbooking extends Component {
    state={
        open2:false,
        uid:'',
        mobile:'',
        user_id:'',
        bookingdetails:[],
        spinner:true,
        starCount: 2.5,
        review:"",
        name:'',
        package_id:''
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Home");
        return true;
      };
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }
    componentWillMount() {
        AsyncStorage.getItem('user')
        .then((value) => {
            if (value != null || value != undefined) {
                const values = JSON.parse(value)
                //console.log('user available =' + values.userid)
                this.setState({ name: values.name, mobile: values.mobile, user_id:values.userid})
                // this.view(values.userid)
                this.viewbooking()
              

            }
        })

    }
    viewbooking(){
        var formData = new FormData();
        formData.append('user_id', this.state.user_id);
        console.log(formData)
        fetch(urldetails.base_url + urldetails.url["my_bookings"],{
            method:"POST",
            body:formData
          }).then((response)=>response.json())
          .then((jsondata)=>{
            console.log(jsondata)
            if(jsondata.status=="success"){
              this.setState({bookingdetails:jsondata.data,spinner:false,open1:false})
                 console.log(this.state.bookingdetails)
                 if(this.state.bookingdetails.length<0)
                 {
                  this.setState({open2:true})
                 }
                            }
                            else{
                                Alert.alert(
                                    'Alert',
                                    "Sorry You have no bookings.If you want to book a boat.Please select a package",
                                    [
                                      {text: 'Ok', onPress: () =>this.props.navigation.navigate("Home")},
                                    ],
                                    { cancelable: false }
                                  )
                            //   this.setState({open2:true})
                            }
          })
          .catch((error) => {
            console.log(error)
            Alert.alert(
              'Alert',
              'Please check your Internet Connection.',
            
            )
          });

    
    }
    review(){
      this.setState({spinner:true})
      var formData = new FormData();
      formData.append('review_name', this.state.name);
      formData.append('review_mobno', this.state.mobile);
      formData.append('review_rating', String(this.state.starCount));
      formData.append('review_msg', this.state.review);
      formData.append('review_package', this.state.package_id);
      console.log(formData)
      fetch(urldetails.base_url + urldetails.url["review"],{
          method:"POST",
          body:formData
        }).then((response)=>response.json()) 
       .then((jsondata)=>{
          console.log(jsondata)
          if(jsondata.status=="success"){
            this.setState({spinner:false,open1:false})
           Alert.alert('Success',jsondata.msg)
                          }
                          else{
                              Alert.alert('Alert',jsondata.msg) 

                          }
        }) .catch((error) => {
          console.log(error)
          Alert.alert(
              'Alert',
              "Please check your Internet Connection.",
              [
                {text: 'Retry', onPress: () =>this.review()},
              ],
              { cancelable: false }
            )
        });
  }
    cancelbooking(id){
console.log(id)
var formData = new FormData();
formData.append('booking_id',id);
console.log(formData)
fetch(urldetails.base_url + urldetails.url["cancel_booking"],{
    method:"POST",
    body:formData
  }).then((response)=>response.json())
  .then((jsondata)=>{
    console.log(jsondata)
    if(jsondata.status=="success"){
        Alert.alert(
            'Message',
            jsondata.msg,
            [
              {text: 'OK', onPress: () =>this.viewbooking()},
            ],
            { cancelable: false }
          )
        this.viewbooking()
    }
    else{
        Alert.alert('',jsondata.msg)
    }
}) .catch((error) => {
    console.log(error)
    Alert.alert(
        'Alert',
        "Please check your Internet Connection.",
        [
          {text: 'Retry', onPress: () =>this.cancelbooking(id)},
        ],
        { cancelable: false }
      )
  });
    }
   
    onStarRatingPress(rating) {
        this.setState({
          starCount: rating
        });
      }
   //modal for nobooking data------------
   modalDidOpen2 = () => console.log("Modal did open");
   modalDidClose2 = () => {
     this.setState({ open2: false });
     console.log("Modal did close.");
   };
   openModal2 = () => this.setState({ open2: true });
   closeModal2 = () => this.setState({ open2: false });
   //-
     render() {      
        return (
        <Container>
                <Header style={{ backgroundColor: '#df3a45' }} >
                    <Left>
                        <TouchableOpacity onPress={()=>this.onBackPress()}>
                            <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>My Bookings</Title></Body>
                </Header>
                <StatusBar backgroundColor="#1a1a1a" barStyle="light-content" />
                <Content style={{backgroundColor:'#FBFCFC'}}>
                {this.state.spinner ? <View style={{ marginTop: screenHeight / 3 }}><Spinner color='red' /></View> : <View />}
                <View  style={{alignContent:'center',alignItems:'center'}}>
                {
                  this.state.bookingdetails.map((data, i) => {
                    return(
                      <Card style={{width:screenwidth-20}} key={i}>
                      <ListItem>
                          <Text style={{ fontSize: 14 }}>BOOKING ID :  </Text>
                          <Text style={{ fontSize: 14, color: "#c81b2a" }}>{data.booking_id}</Text>
                      </ListItem>
                      <CardItem>
                          <Text style={{ fontSize: 14 }}>BOAT NUMBER :</Text>
                          <Text style={{fontSize:14}}>{data.boat_id}</Text>
                      </CardItem>
                      <CardItem>
                          <Text style={{ fontSize: 14 }}>DATE OF RIDE :</Text>
                          <Text style={{fontSize:14}}>{data.date_of_ride}</Text>
                      </CardItem>
                      <CardItem>
                          <Text style={{fontSize:14}}>NUMBER OF PERSON : </Text>
                          <Text style={{fontSize:14}}>{data.num_of_person}</Text>
                      </CardItem>
                      <CardItem>
                          <Text style={{fontSize:14}}>DATE OF BOOKING : </Text>
                          <Text style={{fontSize:14}}>{data.date_of_booking}</Text>
                      </CardItem>
                      <CardItem>
                          <Text style={{fontSize:14}}>BILLING AMOUNT :</Text>
                          <Text style={{fontSize:14}}>{data.total_billing_amount}</Text>
                      </CardItem>
                      {data.status=="CANCEL"?
                      <CardItem>
                          <Text style={{fontSize:14}}>BOOKING STATUS :</Text>
                          <Text style={{fontSize:14,color:'#df3a45'}}>CANCELED</Text>
                      </CardItem>:<View/>}
               
                      {data.status!="CANCEL"?
                <Footer>
                <FooterTab style={{ backgroundColor: '#df3a45'}}>
                {new Date()<(new Date(data.date_of_ride)) && data.status=="CONFIRMED" ?
                  <TouchableOpacity onPress={()=>this.cancelbooking(data.booking_id)}  style={{ backgroundColor: '#df3a45', width: screenwidth-20,}}>
                    <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                    <Text style={{ color: "#fff",paddingTop:3, fontSize: 18 }}>CANCEL</Text>
                    </View>
                  </TouchableOpacity>
                   :
                   <TouchableOpacity onPress={()=>this.setState({open1:true,package_id:data.package_id, mobile:data.mobile_number})}  style={{ backgroundColor: '#df3a45', width: screenwidth-20,}}>
                    <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                    <Text style={{ color: "#fff",paddingTop:3, fontSize: 18 }}>Review</Text>
                    </View>
                  </TouchableOpacity>}
                </FooterTab>
              </Footer>:<View/>}
             
                </Card>   
                    )}
                  )}
                </View>
        </Content> 
        <Modal1
                    offset={0}
                    open={this.state.open1}
                    closeOnTouchOutside={false} disableOnBackPress={true}
                    modalDidOpen={this.modalDidOpen1}
                    modalDidClose={this.modalDidClose1}
                    style={{ alignItems: "center" }}
                >
                  <View style={{flexDirection:'row'}}>
             <Left>
             </Left>
             <Body>
             <Text style={{fontSize:20,color:'#E91E63'}}>Review</Text>
               </Body>
             <Right>
             <TouchableOpacity style={{ backgroundColor: 'red', borderRadius: 25,   width: 25, height: 25 }} onPress={() => this.setState({ open1: false })} >
                  <Text style={{ color: '#fff', fontWeight: 'bold',marginTop:1, padding: 2, textAlign: 'center' }}>X</Text>
                </TouchableOpacity>
             </Right>
           </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ backgroundColor: 'transparent', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <StarRating
        disabled={false}
        emptyStar={'ios-star-outline'}
        fullStar={'ios-star'}
        halfStar={'ios-star-half'}
        iconSet={'Ionicons'}
        maxStars={5}
        rating={this.state.starCount}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
        fullStarColor={'#FFD700'}
      />                    
                        </View>
                        <Textarea style={{ width: '95%', textAlign: 'center' }} rowSpan={5} bordered placeholder="Write Your Review Here" onChangeText={(rev) => this.setState({ review: rev })} />
                        {this.state.spinner ? <View><Spinner color='#ffb400' /></View> : <View />}
                       
                        <TouchableOpacity onPress={() => this.review()} style={{ width: screenwidth - 75, height: 50,  borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                            backgroundColor:'#df3a45',alignItems: "center", justifyContent: 'center',}} 
                        >
                            <Text style={{ color: "white", fontSize: 20 }}>SUBMIT</Text>

                        </TouchableOpacity>
                       
                       
                        {/* <TouchableOpacity  style={{ width: screenwidth - 75, height: 50,  borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                            alignItems: "center", justifyContent: 'center',backgroundColor:'#fff',elevation:8,borderRadius:5,borderColor:'#c81b2a',borderWidth:0.5 }} onPress={() => this.props.navigation.navigate("Viewreview")} 
                        >
                            <Text style={{ color: "white", fontSize: 20 }}>View Review</Text>

                        </TouchableOpacity> */}
                    </View>
                </Modal1>
                <Modal2
            offset={0}
            closeOnTouchOutside={false} disableOnBackPress={true}
            open={this.state.open2}
            modalDidOpen={this.modalDidOpen2}
            modalDidClose={this.modalDidClose2}
            style={{ color: "#F5F5F5" }}>

            {/* <TouchableOpacity style={{ backgroundColor: 'red', borderRadius: 25,   width: 25, height: 25 }} onPress={() => this.setState({ open3: false })} >
                  <Text style={{ color: '#fff', fontWeight: 'bold',marginTop:1, padding: 2, textAlign: 'center' }}>X</Text>
                </TouchableOpacity> */}
                <View style={{alignContent:'center',alignItems:'center',height:screenHeight/3,paddingTop:30}}>
                <Text style={{fontSize:20}}>Sorry You have no bookings.</Text>
                <Text style={{fontSize:20}}>If you want to book a boat.Please click on book Button</Text>
               <View style={{flexDirection:'row',paddingTop:20}}>
               <LinearGradient colors={['#dc2430','#7b4397']} style={{width: screenwidth/3,height:40,backgroundColor:'#c81b2a',borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
 elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}>
  
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("Book")} style={{width: screenwidth/3,height:40,
 alignItems: "center",justifyContent: 'center',elevation: 8,shadowRadius: 5}}>
  <Text style={{color:'#fff',fontSize:20}}>Book Now</Text>
  </TouchableOpacity>
  </LinearGradient>
  <TouchableOpacity onPress={()=>this.props.navigation.navigate("Home")} style={{width: screenwidth/3,height:40,backgroundColor:'#c81b2a',borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
 marginLeft:10,alignItems: "center",justifyContent: 'center',elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}>
  <Text style={{color:'#fff',fontSize:20}}>Cancel</Text>
  </TouchableOpacity>
  </View>
        </View>
        </Modal2>
        </Container>
    );
}
}
