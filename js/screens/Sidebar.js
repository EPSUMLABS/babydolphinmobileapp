import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, Image, View, TouchableOpacity, StatusBar,
  Platform, TouchableWithoutFeedback, Dimensions, AsyncStorage } from 'react-native';
import { Container, Content, Body, Header, Label, ListItem, Thumbnail,Icon } from 'native-base';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import AntIcon from 'react-native-vector-icons/AntDesign';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;

var navitems =[
  {name:'Home', nav:'Home', image:<Icon type="AntDesign" name='home'
  style={{ fontSize: 20, marginTop: 8, marginBottom:8, color: '#e61610' }} />},
  {name:'Profile', nav:'Profile', image:<Icon name='md-person'
  style={{fontSize:20, marginTop:8, marginBottom:8, color:'#e61610'}}/>},
  {name:'Holiday Packages', nav:'Packages', image:<Icon name='md-person'
  style={{fontSize:20, marginTop:8, marginBottom:8, color:'#e61610'}}/>},
  // {name:'Faq',nav:'Faq', image:<Icon type="AntDesign" name='questioncircleo'
  // style={{fontSize:20, marginTop:8, marginBottom:8, color:'#e61610'}}/>},
  {name:'My Bookings', nav:'Viewbooking',image:<Icon type="AntDesign" name='contacts'
  style={{fontSize:20, marginTop:8, marginBottom:8, color:'#e61610'}}/>},
  {name:'Privacy Policy', nav:'Privacypolicy',image:<Icon type="AntDesign" name='contacts'
  style={{fontSize:20, marginTop:8, marginBottom:8, color:'#e61610'}}/>},
  {name:'Terms & Condition', nav:'Termandcondition',image:<Icon type="AntDesign" name='contacts'
  style={{fontSize:20, marginTop:8, marginBottom:8, color:'#e61610'}}/>},
  {name:'About Us',nav:'Aboutus', image:<Icon type="AntDesign" name='infocirlceo'
  style={{fontSize:20, marginTop:8, marginBottom:8, color:'#e61610'}}/>},
  
]

export default class Sidebar extends Component{
  state={
    name:'',
    email:''
  }

  componentWillMount(){
    AsyncStorage.getItem('user')
    .then((value) => {
      const values = JSON.parse(value)
      this.setState({name:values.name, email:values.email});
  });
}

    render() {
        return (
          <Container>
            <View style={{ height: 150, alignItems: "center", justifyContent: 'center', backgroundColor: '#df3a45', }}>
              <Image source={require('../assets/logo.png')} style={{ width: 100, height: 100 }} />
              <Text style={{ color: '#fff', backgroundColor: 'transparent', padding: 4, textAlign: 'center', fontSize: 12 }}>Welcome, {this.state.name}</Text>
            </View>
            <View style={{ borderWidth: 0, flex: 1, backgroundColor: 'white' }}>
              <Content style={{ backgroundColor: '#fff' }}>
                <View>
                  {navitems.map((l, i, ) => {
                    return (<ListItem key={i} style={{ height: 50 }} onPress={() => {
                      this.props.navigation.toggleDrawer(),
                        this.props.navigation.navigate(l.nav)
                    }}>
                      <View style={{ flexDirection: 'row', backgroundColor: '#fff0', marginRight: 50 }}>
                        {l.image}
                        <Text style={{ fontSize: 15, marginLeft: 16, marginTop: 8, color: '#000' }}>{l.name}</Text>
                      </View></ListItem>)
                  })
                  }
                </View>
              </Content>
            </View>
          </Container>
            );
  }
}
