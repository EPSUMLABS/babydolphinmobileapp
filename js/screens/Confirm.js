
import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, StatusBar, 
AsyncStorage, Alert, ImageBackground,BackHandler,ScrollView,Picker,TextInput } from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,Badge,
    Item, Input, Spinner,Card, CardItem
} from 'native-base';
import Modal from "react-native-simple-modal";
import Modal1 from 'react-native-simple-modal';
import Modal2 from 'react-native-simple-modal';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from 'react-native-datepicker'
import urldetails from '../config/endpoints.json';
export default class Ordersummary extends Component {
    state = {
        data:this.props.navigation.state.params.data,
    }
componentWillMount(){
    // console.log(this.state.boatnumber)
}
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };
     render() {
   
        return (
        <Container>
                <Header style={{ backgroundColor: '#df3a45' }} hasTabs>
                    <Left>
                      <TouchableOpacity onPress={()=>this.props.navigation.navigate("Home")}>
                            <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                            <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity> */}
                    </Left>
                    <Body><Title style={{color:'#fff'}}> Order Placed</Title></Body>
                    <Right>
                    </Right>
                </Header>
                <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
             <Content>
                {/* <LinearGradient colors={['#dc2430','#7b4397']}> */}
      <View style={{alignItems:'center',paddingTop:'10%'}}>
      {/* <Image square source={require('../assets/Yes_check.png')} style={{width:60,height:60,}}/> */}
      <Text style={{fontSize:30,color:'#000'}}>
      Congratulations</Text>
        <Text style={{fontSize:25,color:'#000',paddingBottom:20}}>
      Your Booking is Done</Text>
      <Card style={{width:screenwidth-40,height:screenHeight/2.5,backgroundColor:'#fff',elevation:3,paddingTop:5}}>
      <View style={{alignItems:'center',justifyContent:'center'}}>
      <View style={{flexDirection:'row',padding:5,borderBottomColor:'#000',borderBottomWidth:1}}>
      <Text style={{fontSize:18,padding:3}}>YOUR BOAT NUMBER:</Text>
      <Text style={{fontSize:18,padding:3,color:'#000'}}>{this.state.data.boat_number}</Text>
      </View>
       </View>
       <CardItem style={{width:screenwidth-40}}>
       <View style={{flexDirection:'row'}}>
       <Image square source={require('../assets/download.jpeg')} style={{width:100,height:120,padding:10}}/>
       <View style={{paddingLeft:12,paddingTop:5}}>
       <Text style={{fontSize:18,paddingLeft:10,color:'#000'}}>{this.state.data.name.substr(0,16)}</Text>
       <Text style={{fontSize:18,padding:10,}}>BOOKING ID:{this.state.data.booking_id}</Text>
        <Text style={{fontSize:18,paddingLeft:10,}}>DATE OF RIDE:{this.state.data.date_of_ride}</Text>
        <Text style={{fontSize:18,padding:10,}}>PACKAGE NAME :{this.state.data.package_name}</Text>
        </View> 
        </View>
        </CardItem>
        <CardItem style={{flexDirection:'row'}}>
        <View style={{alignContent:'center',alignItems:'center',paddingLeft:13}}>
        <Text style={{fontSize:35,color:'#000'}}>{this.state.data.persons}</Text> 
        <Text style={{fontSize:18,paddingTop:2}}>Person</Text> 
        </View>
        <View style={{alignContent:'center',alignItems:'center',paddingLeft:'25%'}}>
        <Text style={{fontSize:20,color:'#000'}}>TOTAL BILLING</Text> 
        <Text style={{fontSize:22,paddingTop:3}}>₹.{this.state.data.amount}</Text> 
        </View>
        </CardItem>
        </Card>
      </View>
      </Content>
       
        <Footer>
            <FooterTab>
      

          <LinearGradient colors={['#7b4397', '#dc2430']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center"}}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate("Home")} style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}>
                <Text style={{ color: '#fff',textAlign:'center'}}>THANK YOU !</Text>
            </TouchableOpacity>
            </LinearGradient>
         
            </FooterTab>
          </Footer>
      

        
        </Container>
    );
}
}
