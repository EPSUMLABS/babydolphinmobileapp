import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, StatusBar, 
AsyncStorage, Alert, ImageBackground,BackHandler,ScrollView, } from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,Badge,
    Item, Input, Spinner,Card, CardItem,Textarea
} from 'native-base';
import urldetails from '../config/endpoints.json';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import StarRating from 'react-native-star-rating';
import Modal1 from 'react-native-simple-modal';
export default class Bookingdetails extends Component {
    state = {
        bookingdetails: this.props.navigation.state.params.bookingdetails,
        starCount: 2.5,
        review:"",
        uid:'',
        open1:false
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Viewbooking");
        return true;
      };
      componentWillMount() {
        AsyncStorage.getItem('user')
            .then((value) => {
                if (value != null || value != undefined) {
                    const values = JSON.parse(value)
                    console.log('user available' + values.userid)
                    this.setState({ uid: values.userid, name:values.username,email:values.email,contact:values.contact,password:values.password })
                    //this.userProfile()
                    
                }
            })
    }
    onStarRatingPress(rating) {
        this.setState({
          starCount: rating
        });
      }
      review(){
        var formData = new FormData();
        formData.append('user_id', this.state.uid);
        formData.append('rating', this.state.starCount);
        formData.append('remark', this.state.review);
        console.log(formData)
        fetch(urldetails.base_url+urldetails.url["review"],{
            method:"POST",
            body:formData
          }).then((response)=>response.json())
          .then((jsondata)=>{
            console.log(jsondata)
            if(jsondata.status=="success"){
             Alert.alert('',jsondata.message)
             this.setState({open1:false})
                            }
                            else{
                                Alert.alert('',jsondata.message) 
                            }
          })  .catch((error) => {
            console.log(error)
            Alert.alert(
              'Alert',
              'Please check your Internet Connection.',
              [
                { text: 'Retry', }
              ],
              { cancelable: false }
            )
          });
    }

     render() {
   
          
        return (
        <Container>
           
                <Header style={{ backgroundColor: '#df3a45' }} hasTabs>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Viewbooking")}>
                            <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Booking Details</Title></Body>
                    <Right>
        
                      </Right>
                </Header>
                <StatusBar backgroundColor="#1a1a1a" barStyle="light-content" />
                <Content style={{backgroundColor:'#fff'}}>
                <View style={{alignContent:'center',alignItems:'center'}}>
                <Card style={{width:screenwidth-20,elevation:8}}>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>BOOKING ID :  </Text>
                            <Text style={{ fontSize: 16, color: "#c81b2a" }}>{this.state.bookingdetails.book_id}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>NUMBER OF PERSON : </Text>
                            <Text style={{ fontSize: 16 }}>{this.state.bookingdetails.number_of_person}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>DATE OF BOOKING : </Text>
                            <Text style={{ fontSize: 16 }}>{this.state.bookingdetails.date_of_booking}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>DATE OF RIDE :</Text>
                            <Text>{this.state.bookingdetails.date_of_ride}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>BILLING AMOUNT :</Text>
                            <Text>{this.state.bookingdetails.total_billing_amount}</Text>
                        </CardItem>
                  </Card>   
                  {/* <View style={{height: 40,marginTop: 25,marginBottom: 20,justifyContent: 'center',alignItems: 'center',}}>
       <TouchableOpacity  onPress={() => this.setState({open1:true})}  style={{width: screenwidth-40,height: 50,backgroundColor:'#E91E63',borderRadius:5,borderColor: 'rgba(255, 255, 255, 0.5)',
  alignItems: "center",justifyContent: 'center',elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}} 
  >
            <Text style={{ color: "#fff", fontSize: 20 }}>REVIEW</Text>
          </TouchableOpacity>
          </View>  */}
          </View>
        </Content> 
        <Footer>
            <FooterTab style={{ backgroundColor: '#15AE0E',alignContent:'center',alignItems:'center' }}>
             <TouchableOpacity  onPress={() => this.setState({open1:true})}   style={{width: screenwidth,height: 50,backgroundColor:'#15AE0E',
  alignItems: "center",justifyContent: 'center'}} 
  >
           <Text style={{ color: "#fff", fontSize: 20 }}>REVIEW</Text>
          </TouchableOpacity>
         
            </FooterTab>
          </Footer>
        <Modal1
                    offset={0}
                    open={this.state.open1}
                    modalDidOpen={this.modalDidOpen1}
                    modalDidClose={this.modalDidClose1}
                    style={{ alignItems: "center" }}
                >
                      <View style={{flexDirection:'row'}}>
             <Left>
             </Left>
             <Body>
             <Text style={{fontSize:20,color:'#15AE0E'}}>Review</Text>
               </Body>
             <Right>
             <TouchableOpacity style={{ backgroundColor: 'red', borderRadius: 25,   width: 25, height: 25 }} onPress={() => this.setState({ open1: false })} >
                  <Text style={{ color: '#fff', fontWeight: 'bold',marginTop:1, padding: 2, textAlign: 'center' }}>X</Text>
                </TouchableOpacity>
             </Right>
           </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ backgroundColor: 'transparent', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                        <StarRating
        disabled={false}
        emptyStar={'ios-star-outline'}
        fullStar={'ios-star'}
        halfStar={'ios-star-half'}
        iconSet={'Ionicons'}
        maxStars={5}
        rating={this.state.starCount}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
        fullStarColor={'#FFD700'}
      />
                            
                        </View>

                        <Textarea style={{ width: '95%', textAlign: 'center' }} rowSpan={5} bordered placeholder="Write Your Review Here" onChangeText={(rev) => this.setState({ review: rev })} />
                        {this.state.spinner ? <View><Spinner color='#ffb400' /></View> : <View />}
                     
                        <TouchableOpacity onPress={() => this.review()} style={{
                            width: screenwidth - 75, height: 50, backgroundColor: "#15AE0E", borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                            alignItems: "center", justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                        }}
                        >
                            <Text style={{ color: "white", fontSize: 20 }}>SUBMIT</Text>

                        </TouchableOpacity>
                    </View>
                </Modal1>
        
        </Container>
    );
}
}


