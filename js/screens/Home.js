import React, { Component } from 'react';
import {
  View, Text, Image, TouchableOpacity, Dimensions, StatusBar,
  AsyncStorage, Alert, ImageBackground, BackHandler, ScrollView, Linking
} from 'react-native';
import {
  Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
  Separator, Badge,
  Item, Input, Spinner, Card, CardItem,
} from 'native-base';
import ImageSlider from 'react-native-image-slider';
import Imagel from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

import urldetails from '../config/endpoints.json';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-simple-modal';             
import Modal1 from 'react-native-simple-modal';
export default class Home extends Component {
  state = {
    images: [],
    image1: "",
    image2: "",
    image3: "",
    date: '',
    open1: false,
    open:true,
    packagedetails:[]
  }
  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  _onPress = () => {
    this.setState({open1:true})
  }
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    clearInterval(this.countDown);
  }
  onBackPress = () => {
    console.log("back press")
    BackHandler.exitApp()
  };
  componentWillMount() {
    fetch(urldetails.base_url + urldetails.url["banner"], {
      method: "GET"
    }).then((response) => response.json())
      .then((jsondata) => {
        console.log(jsondata.data)
        if (jsondata.status == "success") {
          var arr =[]
          jsondata.data.map((banner,i)=>{
            arr.push(urldetails.image+banner.banner_path)
          })
          this.setState({images: arr})
        }
      })
      fetch(urldetails.base_url + urldetails.url["package"],{
        method:"GET"
      }) .then((response)=>response.json())
      .then((jsondata)=>{
        console.log(jsondata)
        if(jsondata.status=="success"){
         
           this.setState({open:false,packagedetails:jsondata.data})
        }
       
      })  .catch((error) => {
        console.log(error)
        Alert.alert(
          'Alert',
          "Please check your Internet Connection.",
         
          { cancelable: false }
        )
      });
  }
  //modal for selectDate------------
  modalDidOpen = () => console.log("Modal did open");
  modalDidClose = () => {
    this.setState({ open: false });
    console.log("Modal did close.");
  };
  openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });
  //-

  //modal for Package------------
  modalDidOpen1 = () => console.log("Modal did open");
  modalDidClose1 = () => {
    this.setState({ open1: false });
    console.log("Modal did close.");
  };
  openModal1 = () => this.setState({ open1: true });
  closeModal1 = () => this.setState({ open1: false });
  //-
gotodescriptionpage(data){
  this.setState({open1:false})
  this.props.navigation.navigate(("descriptionpage"),{packagedetails:data})
}
  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#df3a45' }} hasTabs>
        <Left>
                <TouchableOpacity transparent onPress={() => this.props.navigation.openDrawer()}>
                  <Icon type="AntDesign" name='bars' style={{ fontSize: 25, color: '#fff' }} />
                </TouchableOpacity>
              </Left>
              <Body>
            <Title style={{width:screenwidth/2,color:'#fff'}}>Baby Dolphin</Title>
          </Body>
          <Right/>
         <Right/>
        </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
        <Content style={{ backgroundColor: '#e6e6e6', }}>
         <ScrollView>
            <View style={{ width: screenwidth, height: screenHeight / 3, padding: 10, alignContent: 'center', alignItems: 'center', backgroundColor:'#fff'}}>
              <ImageSlider autoPlayWithInterval={5000}
                images={this.state.images}
                style={{ width: screenwidth - 12, height: screenHeight / 2 }} />
            </View>
            
            
            <View style={{ width: screenwidth, padding: 10, alignContent: 'center', alignItems: 'center', backgroundColor:'#fff'}}>
              <TouchableOpacity onPress={() =>this.props.navigation.navigate(("Packages"))} style={{backgroundColor:'#df3a45' ,width: screenwidth-10,height: 100,alignContent:'center',alignItems:'center',justifyContent:'center'}}>
                <Text style={{ color: "#fff", fontSize: 20 }} >BOOK NOW</Text>
              </TouchableOpacity>
</View>
            <View
              style={{
                height: screenHeight / 3, width: screenwidth, backgroundColor: '#F0F3F4',
                marginTop: 10,marginBottom:80
              }}>
              <Text style={{ padding: 5, fontSize:10 }}>Explore Chilika</Text>
       <View style={{paddingTop:3,alignContent:'center',alignItems:'center'}}>
       <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
       <TouchableOpacity style={{width:screenwidth/2-15,height:screenHeight/5 }} onPress={()=>this.props.navigation.navigate("KaliJai")}>
       <Image source={require('../assets/Kalijai_temple_Chilika.jpg')} style={{width:screenwidth/2-15,height:screenHeight/5,}}/>
         </TouchableOpacity>
         <TouchableOpacity style={{width:screenwidth/2-15,marginLeft:2,height:screenHeight/5}} onPress={()=>this.props.navigation.navigate("BirdSanctuary")}>
         <Image source={require('../assets/bird.jpg')} style={{width:screenwidth/2-15,height:screenHeight/5,}}/>
         </TouchableOpacity>
         </View>
         </View>
         <View style={{paddingTop:2,alignContent:'center',alignItems:'center'}}>
       <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
       <TouchableOpacity style={{width:screenwidth/2-15,height:screenHeight/5}} onPress={()=>this.props.navigation.navigate("Island")}>
       <Image source={require('../assets/island.jpg')} style={{width:screenwidth/2-15,height:screenHeight/5,}}/>
         </TouchableOpacity>
         <TouchableOpacity style={{width:screenwidth/2-15,marginLeft:2,height:screenHeight/5}} onPress={()=>this.props.navigation.navigate("DolphinPoint")}>
         <Image source={require('../assets/dolphins.jpg')} style={{width:screenwidth/2-15,height:screenHeight/5,}}/>
         </TouchableOpacity>
         </View>
         </View>
            </View>
         </ScrollView>
        </Content>

        <Modal
          offset={0}
          open={this.state.open}
          modalDidOpen={this.modalDidOpen}
          modalDidClose={this.modalDidClose}
          disableOnBackPress={true}
          closeOnTouchOutside={false}
          style={{ alignItems: "center", backgroundColor: "#000", width: screenwidth }}>
          <View style={{ alignContent: 'center', alignItems: 'center' }}>
            <Spinner color='red' />
            <Text style={{textAlign:'center'}}>Welcome To Chilika..</Text>
          </View>
        </Modal>

        <Modal1
          offset={0}
          open={this.state.open1}
          modalDidOpen={this.modalDidOpen1}
          modalDidClose={this.modalDidClose1}
          style={{ alignItems: "center", backgroundColor: "#000", width: screenwidth }}>
          <View style={{ alignContent: 'center', alignItems: 'center' }}>
          <Text style={{textAlign:'center', fontWeight:'bold'}}>PACKAGES</Text>
                    <View>
                    {
                          this.state.packagedetails.map((data, i) => {
                            return(
                    <View
                      style={{
                        height: screenHeight / 3,width:screenwidth-100, backgroundColor: '#F0F3F4',
                        marginTop: 10,
                      }} key={{i}}>
                      
                      <TouchableOpacity style={{ justifyContent: "center", alignContent: 'center', alignItems: 'center', paddingTop: 3 }} 
                      onPress={() =>this.gotodescriptionpage(data)}>
                        <Imagel square source={{ uri: urldetails.image+ data.package_image}} style={{ height: (Dimensions.get("window").height / 4), width: (Dimensions.get("window").width) - 100, }} 
                        indicator={ProgressBar.Pie}
                        indicatorProps={{ size: 80, borderWidth: 0, color: 'rgba(150, 150, 150, 1)', unfilledColor: 'rgba(200, 200, 200, 0.2)' }}/>
                      </TouchableOpacity>
                      <Text style={{ textAlign:'center', padding: 5, fontSize:18,color:'#000' }}>{data.package_name}</Text>
                      <Text style={{ textAlign:'center', padding: 5, fontSize:14 }}>{data.description.substr(0,30)+"..."}</Text>
                    </View>
                           ) })
                        }
                        </View>
                    
          </View>
        </Modal1>

        <Footer>
        <FooterTab style={{ backgroundColor: '#fff' }}>
          <LinearGradient colors={['#7b4397', '#dc2430']} start={{x: 1, y: 0.1}} end={{x: 0.0, y: 0.25}} style={{width:screenwidth,alignItems:"center", justifyContent:"center",flexDirection:'row'}}>
            <TouchableOpacity  onPress={() =>this.props.navigation.navigate(("Packages"),
                  {packagedetails:this.state.packagedetails})}style={{flexDirection:'row', alignItems:"center", justifyContent:"center"}}>
                <Text style={{ color: '#fff',textAlign:'center'}}>Plan Your Holiday</Text>
                <Icon style={{ color: '#fff',  alignItems:'center', paddingLeft:10 }} name='ios-arrow-round-forward' />
            </TouchableOpacity>
            </LinearGradient>
          </FooterTab>
        </Footer>
      </Container>

    );
  }
}


