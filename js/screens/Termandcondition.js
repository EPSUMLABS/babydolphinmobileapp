

import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions,
    AsyncStorage,
    TouchableOpacity,
    StatusBar, Alert,
    ImageBackground,
    BackHandler,Linking
  } from 'react-native';
  import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator, Badge,
    Item, Input, Spinner, Card, CardItem,
  } from 'native-base';
  import LinearGradient from 'react-native-linear-gradient';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class Privacypolicy extends Component {
    state = {
        spinner: true,
      };
      componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      }
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
        clearInterval(this.countDown);
      }
      onBackPress = () => {
        this.props.navigation.navigate("Home");
        return true;
      };
  render() {
    return (
    <Container>
          <Header style={{ backgroundColor:'#df3a45' }} >
          <Left>
          <Icon name='arrow-back' onPress={()=>this.props.navigation.navigate("Home")}
             style={{ fontSize: 25, color: '#fff' }} />
          </Left>
          <Body><Title>Terms & Conditions</Title></Body>
        </Header>
        <StatusBar backgroundColor="#9a1922" barStyle="light-content" />
     <Content>
   <View style={{justifyContent:'center',alignItems:'center',marginTop:10}}>
  <Text style={{fontSize:20,padding:8,}}>
These Terms will be applied fully and affect to your use of this Website. By using this Website, you agreed to accept all terms and conditions written in here. You must not use this Website if you disagree with any of these Website Standard Terms and Conditions.
</Text>
<Text  style={{fontSize:20,padding:8,}}>

Other than the content you own, under these Terms, Babydolphin and/or its licensors own all the intellectual property rights and materials contained in this Website.


</Text>
<Text  style={{fontSize:20,padding:8}}>
    
You are specifically restricted from all of the following:

publishing any Website material in any other media;
selling, sublicensing and/or otherwise commercializing any Website material;
publicly performing and/or showing any Website material;
using this Website in any way that is or may be damaging to this Website;
using this Website in any way that impacts user access to this Website;
using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;
engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website;
using this Website to engage in any advertising or marketing.
</Text>
<Text  style={{fontSize:20,padding:8,}}>
Refund Policy
You can cancel the ride one day before the date of ride. Rs 100/- will be deducted as the cancellation charge. Remaning amount will be refunded tou your account within in 7 working days.


By using our website, you hereby consent to our Privacy Policy and agree to its Terms and Conditions.


    </Text>
   </View> 
   </Content> 
    </Container>
  );
}
}



 

